FROM golang:1.19.1-alpine3.16 AS builder

WORKDIR /app

COPY . .

RUN go build -o main cmd/main.go
 
FROM alpine:3.16

WORKDIR /app
RUN mkdir media

COPY --from=builder /app/main .
COPY templates ./templates

EXPOSE 8000

CMD ["/app/main"]

# docker run --env-file ./.env --name blogApp --network blog-network -p 8000:8000 -d blog:latest
